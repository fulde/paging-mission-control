class Component:
	def __init__(self, name, sat_id, red_high, yellow_high, yellow_low, red_low):
		"""
		Constructor for Component class
		Parameters:
		  - name (str) : name of the component
		  - sat_id (int) : ID for the satellite
          - red_high (int) : value for the red high limit
          - yellow_high (int) : value for the yellow high limit
		  - yellow_low (int) : value for the yellow low limit
		  - red_low (int) : value for the red low limit
		"""
		self.name = name
		self.sat_id = sat_id
		self.red_high = red_high
		self.yellow_high = yellow_high
		self.yellow_low = yellow_low
		self.red_low = red_low

		# map class attributes to "severity" values for alert output 
		self.limit_name_severity_mapping = {
			'red_high': 'RED HIGH',
			'yellow_high': 'YELLOW HIGH',
			'yellow_low': 'YELLOW LOW',
			'red_low': 'RED LOW'
		}

	def get_severity_output_string(self, limit_name):
		"""
		Returns output string for a given limit name
		Parameters:
		  - limit_name : limit to get output string for
		"""
		return self.limit_name_severity_mapping[limit_name]