import operator
import pytest

from ingestion_manager import IngestionManager

def ingest_data(filename):
	manager = IngestionManager()
	with open(filename) as f:
		line = f.readline()
		while line:
			manager.parse_metric(line)
			line = f.readline()
	return manager


def test_file1():
	manager = ingest_data('test_input/file1.txt')
	output = []
	for satellite in manager.get_satellites():
		# check for three TSTAT values over satellite's red_high limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'red_high', operator.gt)
		if alert:
			output.append(alert)

		# check for three BATT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('BATT', 'red_low', operator.lt)
		if alert:
			output.append(alert)

		# check for three TSTAT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'yellow_high', operator.gt)
		if alert:
			output.append(alert)

	output.sort(key=lambda alert:alert['timestamp'])
	assert output == [
		{'timestamp': '2018-01-01T23:01:09.521Z', 'component': 'BATT', 'severity': 'RED LOW', 'satelliteId': 1000},
		{'timestamp': '2018-01-01T23:01:38.001Z', 'component': 'TSTAT', 'severity': 'RED HIGH', 'satelliteId': 1000},
		{'timestamp': '2018-01-01T23:01:38.001Z', 'component': 'TSTAT', 'severity': 'YELLOW HIGH', 'satelliteId': 1000}
	]


def test_file2():
	manager = ingest_data('test_input/file2.txt')
	output = []
	for satellite in manager.get_satellites():
		# check for three TSTAT values over satellite's red_high limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'red_high', operator.gt)
		if alert:
			output.append(alert)

		# check for three BATT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('BATT', 'red_low', operator.lt)
		if alert:
			output.append(alert)

		# check for three TSTAT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'yellow_high', operator.gt)
		if alert:
			output.append(alert)

	output.sort(key=lambda alert:alert['timestamp'])
	assert output == [
		{'satelliteId': 1101, 'severity': 'YELLOW HIGH', 'component': 'TSTAT', 'timestamp': '2019-11-04T05:21:26.011Z'},
		{'satelliteId': 1000, 'severity': 'RED LOW', 'component': 'BATT', 'timestamp': '2019-11-04T23:01:09.521Z'},
		{'satelliteId': 1000, 'severity': 'RED HIGH', 'component': 'TSTAT', 'timestamp': '2019-11-04T23:01:38.001Z'},
		{'satelliteId': 1000, 'severity': 'YELLOW HIGH', 'component': 'TSTAT', 'timestamp': '2019-11-04T23:01:38.001Z'}
	]


def test_file3():
	manager = ingest_data('test_input/file3.txt')
	output = []
	for satellite in manager.get_satellites():
		# check for three TSTAT values over satellite's red_high limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'red_high', operator.gt)
		if alert:
			output.append(alert)

		# check for three BATT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('BATT', 'red_low', operator.lt)
		if alert:
			output.append(alert)

		# check for three TSTAT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'yellow_high', operator.gt)
		if alert:
			output.append(alert)

	output.sort(key=lambda alert:alert['timestamp'])
	assert output == [
		{'timestamp': '2018-01-01T23:01:09.521Z', 'component': 'BATT', 'severity': 'RED LOW', 'satelliteId': 1000},
		{'timestamp': '2018-01-01T23:01:38.001Z', 'component': 'TSTAT', 'severity': 'RED HIGH', 'satelliteId': 1000},
		{'timestamp': '2018-01-01T23:01:38.001Z', 'component': 'TSTAT', 'severity': 'YELLOW HIGH', 'satelliteId': 1000}
	]
