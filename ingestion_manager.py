import datetime

from satellite import Satellite
from event import Event
from component import Component


class IngestionManager:
	def __init__(self):
		"""
		Constructor for IngestionManager class
		"""
		self._satellites = {}

	def get_satellites(self):
		"""
		Get list of all satellites ingested
		Parameters:
		  none
		Returns:
		  list of Satellite objects
		"""
		return list(self._satellites.values())

	def parse_metric(self, metric):
		"""
		Parse one metric into Satellite, Event, and Component objects and
		   incorporate with ingested data

		Parameters:
		  - metric (str) : single line of metric data
		Returns:
		  nothing
		"""
		data = metric.strip().split('|')
		sat_id = data[1]

		timestamp = datetime.datetime.strptime(data[0], '%Y%m%d %H:%M:%S.%f')
		event = Event(sat_id, data[7], timestamp, float(data[6]))
		component = Component(data[7], sat_id, int(data[2]), int(data[3]), int(data[4]), int(data[5]))

		if sat_id not in self._satellites:
			self._satellites[sat_id] = Satellite(int(data[1]))
		self._satellites[sat_id].add_event(event)
		self._satellites[sat_id].add_component(component)
