import sys
import argparse
import datetime
import operator
import json

from ingestion_manager import IngestionManager

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Alert parser for satellite telemetry data')
	parser.add_argument(
		'filename',
		action='store',
		help='Name of the file to parse for metric data')

	args = parser.parse_args()
	manager = IngestionManager()
	with open(args.filename) as f:
		try:
			line = f.readline()
			while line:
				manager.parse_metric(line)
				line = f.readline()
		except Exception as e:
			print("Error: exception encountered while reading the given file")
			print(e)
			sys.exit(1)

	output = []
	for satellite in manager.get_satellites():
		# check for three TSTAT values over satellite's red_high limit within five-minute interval
		alert = satellite.test_for_alert('TSTAT', 'red_high', operator.gt)
		if alert:
			output.append(alert)

		# check for three BATT values over satellite's red_low limit within five-minute interval
		alert = satellite.test_for_alert('BATT', 'red_low', operator.lt)
		if alert:
			output.append(alert)

	print(json.dumps(output))