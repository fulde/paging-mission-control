import datetime
import operator

from event import Event
from component import Component

class Satellite:
	def __init__(self, sat_id):
		"""
		Constructor for IngestionManager class
		Parameters:
		  - sat_id (int) : ID for the satellite
		"""
		self.sat_id = sat_id

		self._events = []
		self._components = {}


	def add_event(self, event, post_sort=True):
		"""
		Add an Event object to list of events
		Parameters:
		  - event (Event): event object to add
		  - post_sort (bool) : sort events by timestamp after adding (default True)
		"""
		self._events.append(event)
		if post_sort:
			self._events.sort(key=lambda event:event.timestamp)

	def add_component(self, component):
		"""
		Add a Component object to dict of components, organized by component name
		Parameters:
		  - component (Component): component object to add
		"""
		self._components[component.name] = component

	def get_events_for_component(self, component):
		"""
		Get list of events on this satellite for a particular component
		Parameters:
		  - component (str): desired component to get events for
		Returns:
		  list of Events
		"""
		return [event for event in self._events if event.component_name == component]

	def test_for_alert(self, component_name, limit_attr, comparator, incidence_threshold=3, time_interval=300):
		"""
		Check Satellite's events for an alert under supplied constraints
		Parameters:
		  - component (str) : name of the component to check metrics for
		  - limit_attr (str) : attribute of the Component class to compare to metric values
		  - comparator (operator) : function for comparing metric values to desired limit
		  - incidence_threshold (int) : number of violations needed to generate an alert  
		  - time_interval (int) : time window in seconds in which the incidence threshold must be reached to generate an alert
		Returns:
		  JSON with alert data, or None if no alert
		"""

		if component_name not in self._components:
			return None

		component = self._components[component_name]
		limit_value = getattr(component, limit_attr)

		# get all events for given component and filter out events without violations
		all_component_events = self.get_events_for_component(component.name)
		violation_events = [event for event in all_component_events if comparator(event.value, limit_value)]

		# not enough events to qualify as an alert
		if len(violation_events) < incidence_threshold:
			return None

		# events are sorted by time
		# slide a "window" through the events list and see if the time window is within the given interval
		for i in range(len(violation_events) - incidence_threshold + 1):
			window = violation_events[i:i + incidence_threshold]
			time_diff = window[-1].timestamp - window[0].timestamp	# time diff between newest and oldest event
			if time_diff.total_seconds() <= time_interval:
				return {
					"satelliteId": self.sat_id,
					"severity": component.get_severity_output_string(limit_attr),
					"component": component.name,
					"timestamp": window[0].timestamp.isoformat()[:-3] + 'Z'
				}

		return None
