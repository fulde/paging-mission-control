import datetime

class Event:
	def __init__(self, sat_id, component_name, timestamp, value):
		"""
		Constructor for Event class
		Parameters:
		  - sat_id (int) : ID for the satellite
		  - component_name (str) : name of the component that recorded this event
		  - timestamp (datetime) : timestamp when the event was recorded
		  - value (float) : value of the measurement
		"""
		self.sat_id = sat_id
		self.component_name = component_name
		self.timestamp = timestamp
		self.value = value
